#!/bin/bash
echo "Building root filesystem on /target/"
ROOT=$1
mkdir $ROOT/{bin, sbin, etc, lib, usr, usr/bin, usr/lib, usr/sbin, var, root, home}
cd $ROOT
echo "Building symbolic links"
ln -s lib lib64
echo "Setting up stage2 environment"
cp /bin/bash $ROOT/bin/
echo "Moving scripts to /target/"
cp install.sh $ROOT/installer.sh
echo "Downloading packages..."
./getrootpkg.sh $ROOT/tmp
echo "Done!"
echo "Entering stage2"
chroot /target/ /installer.sh
